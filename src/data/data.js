const HEADER_DATA = [
    { id: 1, url: "/", label: "Pizza" },
    { id: 2, url: "#services", label: "Popular" },
    { id: 3, url: "#about-us", label: "Salads" },
    { id: 4, url: "#testimonials", label: "Hot meals" },
    { id: 5, url: "#footer", label: "Drinks" },
    { id: 6, url: "#deserts", label: "Deserts" },
    { id: 7, url: "#sauces", label: "Sauces" }
    

  ];

  const FOOTER_DATA = {
    DESCRIPTION:
      "We are typically focused on result-based maketing in the digital world. Also, we evaluate your brand’s needs and develop a powerful strategy that maximizes profits.",
    CONTACT_DETAILS: {
      HEADING: "Contact us",
      ADDRESS: "La trobe street docklands, Melbourne",
      MOBILE: "+1 61234567890",
      EMAIL: "nixalar@gmail.com"
    },
    SUBSCRIBE_NEWSLETTER: "Subscribe newsletter",
    SUBSCRIBE: "Subscribe"
  };

  const FOOTER = [
   '<i class="fa fa-list-alt" aria-hidden="true"></i>',
   '<i class="fa fa-gift" aria-hidden="true"></i>',
   '<i class="fa fa-shopping-cart" aria-hidden="true"></i>',
   '<i class="fa fa-file-text-o" aria-hidden="true"></i>',
   '<i class="fa fa-circle-o" aria-hidden="true"></i>'
  ];
  
  const MENUBLOCKS = [];
  const COLOR = ['#56c01aa6', '#1a7ac0a6', '#c48923a6', '#c421a9a6', '#5d4149a6'];
  const MOCK_DATA = {
    HEADER_DATA,
    FOOTER_DATA,
    MENUBLOCKS,
    COLOR,
    FOOTER
  }
  
  export default MOCK_DATA;